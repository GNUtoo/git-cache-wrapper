/* Copyright (C) 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "git.h"

/*
 * git [-v | --version] [-h | --help] [-C <path>] [-c <name>=<value>]
 * [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
 * [-p|--paginate|-P|--no-pager] [--no-replace-objects] [--bare]
 * [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
 * [--super-prefix=<path>] [--config-env=<name>=<envvar>]
 * <command> [<args>]
 */
static struct option git_long_options[] = {
	/* [-v | --version] */
	{"version", no_argument, 0, 'v' },
	{"help", no_argument, 0, 'h' },
	// [-C <path>]
	// [-c <name>=<value>]
	{"exec-path", required_argument, 0, 0}, /*  [--exec-path[=<path>]] */
	{"html-path", no_argument, 0, 0}, /*  [--html-path] [--man-path] */
	{"man-path", no_argument, 0, 0}, /* [--man-path]  */
	{"info-path", no_argument, 0, 0}, /* [--info-path] */
	{"paginate", no_argument, 0, 'p'}, /* [-p|--paginate|-P|--no-pager] */
	{"no-pager", no_argument, 0, 'P'}, /* [-p|--paginate|-P|--no-pager] */
	{"no-replace-objects", no_argument, 0, 0}, /* [--no-replace-objects] */
	{"bare", no_argument, 0, 0}, /* [--bare] */
	{"git-dir", required_argument, 0, 0}, /* [--git-dir=<path>] */
	{"work-tree", required_argument, 0, 0}, /* [--work-tree=<path>] */
	{"namespace", required_argument, 0, 0}, /* [--namespace=<name>] */
	{"super-prefix", required_argument, 0, 0}, /* [--super-prefix=<path>] */
	{"config-env", required_argument, 0, 0}, /* [--config-env=<name>=<envvar>] */
	/* <command> [<args>] */
	{0, 0, 0, 0 }
};

/* [--template=<template-directory>]
 * [-l] [-s] [--no-hardlinks] [-q] [-n] [--bare] [--mirror]
 * [-o <name>] [-b <name>] [-u <upload-pack>] [--reference <repository>]
 * [--dissociate] [--separate-git-dir <git-dir>]
 * [--depth <depth>] [--[no-]single-branch] [--no-tags]
 * [--recurse-submodules[=<pathspec>]] [--[no-]shallow-submodules]
 * [--[no-]remote-submodules] [--jobs <n>] [--sparse] [--[no-]reject-shallow]
 * [--filter=<filter> [--also-filter-submodules]] [--] <repository>
 * [<directory>]
 */
static struct option clone_long_options[] = {
	/* [--template=<template-directory>] */
	{"template", required_argument, 0, 0 },

	/* [--no-hardlinks] */
	{"no-hardlinks", no_argument, 0, 0 },

	/* [--bare] */
	{"bare", no_argument, 0, 0 },

	/* [--mirror] */
	{"mirror", no_argument, 0, 0 },

	/* [--reference <repository>] */
	{"references", required_argument, 0, 0 },

	/* [--dissociate] */
	{"dissociate", no_argument, 0, 0 },

	/* [--separate-git-dir <git-dir>] */
	{"separate-git-dir", required_argument, 0, 0 },

	/* [--depth <depth>] */
	{"depth", required_argument, 0, 0 },

	/* [--[no-]single-branch] */
	{"single-branch", no_argument, 0, 0 },

	/* [--[no-]single-branch] */
	{"no-single-branch", no_argument, 0, 0 },

	/* [--no-tags] */
	{"no-tags", no_argument, 0, 0 },

	/* [--recurse-submodules[=<pathspec>]] */
	{"recurse-submodules", required_argument, 0, 0 },

	/* [--[no-]shallow-submodules] */
	{"shallow-submodules", no_argument, 0, 0 },

	/* [--[no-]shallow-submodules] */
	{"no-shallow-submodules", no_argument, 0, 0 },

	/* [--[no-]remote-submodules] */
	{"remote-submodules", no_argument, 0, 0 },

	/* [--[no-]remote-submodules] */
	{"no-remote-submodules", no_argument, 0, 0 },

	/* [--jobs <n>] */
	{"jobs", required_argument, 0, 0 },

	/* [--sparse] */
	{"sparse", no_argument, 0, 0 },

	/* [--[no-]reject-shallow] */
	{"no-tags", no_argument, 0, 0 },

	/* [--filter=<filter> [--also-filter-submodules]] [--] <repository> */
	{"filter", required_argument, 0, 0 },

	/* [--filter=<filter> [--also-filter-submodules]] [--] <repository> */
	{"also-filter-submodules", no_argument, 0, 0 },

	/* [<directory>] */
};

/* This parses the arguments after git [...] clone */
bool parse_clone_args(int argc, char *const argv[], struct cmdline *cmdline)
{
	const char *clone_optstring = "lsqno:b:u:";
	int opterr_save = opterr;

	if (argc < 1)
		return false;

	while (true) {
		char c;

		opterr = 0;
		c = getopt_long(argc, argv, clone_optstring, clone_long_options, NULL);
		opterr = opterr_save;

		if (c == -1)
			break;
	}

	cmdline->repository_index = optind;
	if (argc > cmdline->repository_index)
		cmdline->directory_index = optind + 1;

	return true;
}

/* This parses the git arguments */
bool parse_git_args(int argc, char *const argv[], struct cmdline *cmdline)
{
	char *git_optstring = "vhC:c:pP";
	int opterr_save = opterr;

	if (argc < 1)
		return false;

	while (true) {
		char c;

		opterr = 0;
		c = getopt_long(argc, argv, git_optstring, git_long_options, NULL);
		opterr_save = opterr;

		if (c == -1)
			break;
	}

	if (argc > optind)
		if (!strcmp("clone", argv[optind]))
			cmdline->clone_index = optind;

	return true;
}

bool parse_args(int argc, char *const argv[], struct cmdline *cmdline)
{
	bool status;

	status = parse_git_args(argc, argv, cmdline);
	if (status == false)
		return status;

	if (cmdline->clone_index > 0 && argc > cmdline->clone_index) {
		/* We pass it the arguments after "clone" */
		status = parse_clone_args(argc - cmdline->clone_index - 1,
					  &(argv[cmdline->clone_index + 1]),
					  cmdline);
		/* parse_clone_args will only give the index
		 * respective to after the "clone" argument, so we
		 * need to be relative to argv start.
		 */
		cmdline->repository_index += cmdline->clone_index;
		if (cmdline->directory_index)
			cmdline->directory_index += cmdline->clone_index;

		if (status == false)
			return status;
	}

	return true;
}

bool add_args(int argc, char *const argv[], struct cmdline *cmdline)
{
	/* git [...] clone --mirror [...] <> <> */
}

bool print_argv(int argc, char *const argv[], char *message)
{
	int i;
	char *string;

	if (message && strlen(message) > 0)
		printf("%s: [ ", message);
	else
		printf("[ ");
	for (i = 0 ; i < argc ; i++)
		printf("%s ", argv[i]);
	printf("]\n");
}

void print_cmdline(struct cmdline *cmdline, char *message)
{
	if (message && strlen(message) > 0)
		printf("%s: { "
		       "clone_index: %d, "
		       "repository_index: %d, "
		       "directory_index: %d }\n",
		       message,
		       cmdline->clone_index,
		       cmdline->repository_index,
		       cmdline->directory_index);
	else
		printf("{ "
		       "clone_index: %d, "
		       "repository_index: %d, "
		       "directory_index: %d }\n",
		       cmdline->clone_index,
		       cmdline->repository_index,
		       cmdline->directory_index);
}
