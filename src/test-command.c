/* Copyright (C) 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <string.h>
#include <strings.h>

#include "git.h"

bool is_clone(int argc, char * const argv[])
{
	struct cmdline cmdline;

	bzero(&cmdline, sizeof(cmdline));

	parse_git_args(argc, argv, &cmdline);
	if ((cmdline.clone_index) > 0)
		return true;

	return false;
}

bool has_repository(int argc, char * const argv[], const char *repository)
{
	struct cmdline cmdline;

	bzero(&cmdline, sizeof(cmdline));

	parse_clone_args(argc, argv, &cmdline);

	if ((cmdline.repository_index) <= 0)
		return false;

	if (strcmp(repository, argv[cmdline.repository_index]))
		return false;

	return true;
}

bool test_git_clone_framagit(void)
{
	struct cmdline cmdline;
	bool result;

	static char * const argv[] = {
		"git", "clone", "https://framagit.org/foo/bar.git"
	};

	bzero(&cmdline, sizeof(cmdline));

	result = parse_args(3, argv, &cmdline);
	if (!result)
		return false;

	if ((cmdline.clone_index) <= 0)
		return false;

	if (strcmp("https://framagit.org/foo/bar.git",
		   argv[cmdline.repository_index]))
		return false;

	return true;
}

bool test_git_plone_framagit(void)
{
	bool result;

	static char * const command[] = {
		"git", "plone", "https://framagit.org/foo/bar.git"
	};

	result = is_clone(3, command);
	CHECK_RESULT(
		!result,
		"is_clone([\"git\", \"plone\", \"https://framagit.org/foo/bar.git\"]);");

	result = has_repository(1, &(command[2]), "https://framagit.org/foo/bar.git");
	CHECK_RESULT(
		!result,
		"has_repository([\"https://framagit.org/foo/bar.git\"]);");

	return true;
}

bool test_git(void)
{
	bool result;

	static char * const command[] = { "git" };

	result = is_clone(1, command);
	CHECK_RESULT(!result, "is_clone([\"git\"]);");
}

bool test_no_command(void)
{
	CHECK_RESULT(!is_clone(0, NULL), "is_clone([]);");
}

bool test_clone_args(void)
{
	char * const argv[] = {
		"--bare", "--mirror",
		"framagit.org/foo/bar.git"
	};
	struct cmdline cmdline;
	bool status;

	bzero(&cmdline, sizeof(cmdline));
	status = parse_clone_args(3, argv, &cmdline);

	return true;
}

bool test_command(void)
{
	bool result;

	result = test_git();
	CHECK_RESULT(result, "test_git();");

	result = test_no_command();
	CHECK_RESULT(result, "test_no_command();");

	result = test_git_clone_framagit();
	CHECK_RESULT(result, "test_git_clone_framagit();");

	result = test_git_plone_framagit();
	CHECK_RESULT(result, "test_git_plone_framagit();");

	return result;
}
