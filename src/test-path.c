/* Copyright (C) 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "git.h"

bool test_compute_path(void)
{
	bool result;
	char *path;

	path = compute_path("https://framagit.org/foo/bar.git");
	if (path == NULL)
		return false;
	result = !strcmp("/srv/git/framagit.org/foo/bar.git", path);
	free(path);
	CHECK_RESULT(result,
		     "compute_path(\"https://framagit.org/foo/bar.git\");");

	path = compute_path("");
	if (path == NULL)
		return false;

	result = !strcmp("", path);
	free(path);
	CHECK_RESULT(result, "compute_path(\"\");");

	return true;
}

bool test_extract_framagit(void)
{
	bool ok;
	char *repository = "https://framagit.org/foo/bar.git";
	struct repository result;

	ok = extract_all(repository, &result);
	if (!ok)
		return false;

	if (result.protocol != HTTPS)
		goto fail;

	if (result.protocol_len != strlen("https://"))
		goto fail;

	if (strcmp("framagit.org", result.domain))
		goto fail;

	if (strcmp("foo/bar", result.path))
		goto fail;

	free_repository(&result);
	return true;

fail:
	free_repository(&result);
	return false;
}

bool test_extract_foobar_protocol(void)
{
	bool ok;
	char *repository = "foobar://framagit.org/foo/bar.git";
	struct repository result;

	ok = extract_all(repository, &result);
	if (ok) {
		free_repository(&result);
		return false;
	}

	if (result.protocol == HTTPS)
		return false;

	return true;
}

bool test_extract_string_len_0(void)
{
	bool ok;
	char *repository = "";
	struct repository result;

	ok = extract_all(repository, &result);
	if (ok) {
		free_repository(&result);
		return false;
	}

	return true;
}

bool test_extract_no_domain(void)
{
	bool ok;
	char *repository = "https://";
	struct repository result;

	ok = extract_all(repository, &result);
	if (ok) {
		free_repository(&result);
		return false;
	}

	return true;
}

bool test_extract_no_slash_in_domain(void)
{
	bool ok;
	char *repository = "https://framagit.org";
	struct repository result;

	ok = extract_all(repository, &result);
	if (ok) {
		free_repository(&result);
		return false;
	}

	return true;
}

bool test_extract_no_path(void)
{
	bool ok;
	char *repository = "https://framagit.org/";
	struct repository result;

	ok = extract_all(repository, &result);
	if (ok) {
		free_repository(&result);
		return false;
	}

	return true;
}
bool test_extract_all(void)
{
	bool ok;

	ok = test_extract_framagit();
	CHECK_RESULT(ok, "test_extract_framagit");

	ok = test_extract_foobar_protocol();
	CHECK_RESULT(ok, "test_extract_foobar_protocol");

	ok = test_extract_string_len_0();
	CHECK_RESULT(ok, "test_extract_string_len_0");

	ok = test_extract_no_domain();
	CHECK_RESULT(ok, "test_extract_no_domain");

	ok = test_extract_no_slash_in_domain();
	CHECK_RESULT(ok, "test_extract_no_slash_in_domain");

	ok = test_extract_no_path();
	CHECK_RESULT(ok, "test_extract_no_path");

	return true;
}

bool test_path(void)
{
	bool ok;

	ok = test_extract_all();
	CHECK_RESULT(ok, "test_extract_all");

	ok = test_compute_path();
	CHECK_RESULT(ok, "test_compute_path");

	return true;
}
