/* Copyright (C) 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "git.h"

extern char **environ;

void *mkdir(char *path)
{
	char *repository_path;
	char *parent_dir_path;

	/* dirname modifies the string given to it */
	repository_path = strdup(path);
	assert(repository_path);
	parent_dir_path = dirname(repository_path);
	printf("mkdir -p %s\n", repository_path);
	free(repository_path);
}

void print_command(int argc, char *const argv[], struct cmdline *cmd)
{
		int i;
		char *dir;
		char *mirror_path;
		char *repository;
		bool status;

		status = parse_args(argc, argv, cmd);
		if (!status) {
			printf("parse_args failed\n");
			exit(-1);
		}
		mirror_path = compute_path(argv[cmd->repository_index]);

		mkdir(mirror_path);

		printf("/run/current-system/profile/bin/git ");
		for (i = 1; i < argc ; i++) {
			if (cmd->directory_index == 0 ||
			    i != cmd->directory_index)
				printf("%s", argv[i]);

			if (i != (argc - 1))
				printf(" ");

			if (i == cmd->clone_index)
				printf("--mirror ");

			if (i == argc -1)
				printf(" %s",
				       (mirror_path) ? mirror_path : "NULL");
		}
		printf("\n");

		printf("/run/current-system/profile/bin/git ");
		for (i = 1; i < argc; i++) {
			printf("%s", argv[i]);
			if (i != (argc - 1))
				printf(" ");
			if (i == cmd->clone_index) {
				printf("--shared ");
				printf("--reference ");
				printf("%s ",
				       mirror_path ? mirror_path : "NULL");
			}
		}
		printf("\n");

		if (mirror_path)
			free(mirror_path);

}

int run(int argc, char *const argv[])
{
	char *command = NULL;
	int i;
	ssize_t offset = 0;
	int rc;
	ssize_t size = 0;

	for (i = 0; i < argc; i++) {
		if (i != 0)
			offset = size;

		/* We either need space for a '\0' (when i == argc -
		 * 1) or space for a space in all other cases.
		 */
		size += strlen(argv[i]) + 1;

		command = realloc(command, size);
		assert(command);
		strcpy(command + offset, argv[i]);
		if (i == (argc -1))
			command[size - 1] = '\0';
		else
			command[size - 1] = ' ';
	}

	rc = system(command);
	free(command);
	return rc;
}

int main(int argc, char *const argv[])
{
	struct cmdline cmd;
	char **saved_argv;
	bool status;

	bzero(&cmd, sizeof(cmd));

	/* argv is modified later by getopts */
	saved_argv = calloc(argc, sizeof(char*));
	assert(saved_argv);
	memcpy(saved_argv, argv, argc * sizeof(char*));

	status = parse_git_args(argc, argv, &cmd);
	if (!status || cmd.clone_index == 0) {
		saved_argv[0] = "/run/current-system/profile/bin/git";
		return run(argc, saved_argv);
	} else {
		print_command(argc, saved_argv, &cmd);
	}

	return 0;
}
