/* Copyright (C) 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GIT_WRAPPER_H
#define GIT_WRAPPER_H

#include <stdbool.h>
#include <stdio.h>

enum protocols {
	UNKNOWN,
	GIT,
	HTTP,
	HTTPS,
};

#define CHECK_RESULT(boolean, name)					\
	do {								\
		if (!boolean) {						\
			printf("[ !! ] %s\n", name);			\
			return false;					\
		} else {						\
			printf("[ OK ] %s\n", name);			\
		}							\
	} while (0)							\

/* command.c */
struct cmdline {
	ssize_t clone_index;
	ssize_t repository_index;
	ssize_t directory_index;
};
bool parse_args(int argc, char *const argv[], struct cmdline *cmdline);
bool parse_clone_args(int argc,  char *const argv[], struct cmdline *cmdline);
bool parse_git_args(int argc, char *const argv[], struct cmdline *cmdline);
bool print_argv(int argc, char *const argv[], char *message);
void print_cmdline(struct cmdline *cmdline, char *message);

/* path.c */
struct repository {
	int protocol;
	ssize_t protocol_len;
	char *domain;
	ssize_t domain_len;
	char *path;
	ssize_t path_len;
};
char *compute_path(char *repository);
bool extract_all(char *repository, struct repository *result);
void free_repository(struct repository *repository);

/* test-command.c */
bool is_clone(int argc, char *const argv[]);
bool test_command(void);

/* test-path.c */
bool test_path(void);

#endif /* GIT_WRAPPER_H */
